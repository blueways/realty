<?php
/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * This class renders the heatmap.
 *
 * @package TYPO3
 * @subpackage tx_realty
 *
 * @author Maik Schneider <m.schneider@blueways.de>
 */
class tx_realty_pi1_HeatmapView extends tx_realty_pi1_FrontEndView {
    /**
     * @var bool whether the constructor is called in test mode
     */
    private $isTestMode = FALSE;


    /**
     * The constructor.
     *
     * @param array $configuration TypoScript configuration for the plugin
     * @param tslib_cObj $cObj the parent cObj content, needed for the flexforms
     * @param bool $isTestMode whether the class is instantiated in test mode
     */
    public function __construct(
        array $configuration, tslib_cObj $cObj, $isTestMode = FALSE
    ) {
        parent::__construct($configuration, $cObj, $isTestMode);
        $this->isTestMode = $isTestMode;
    }

    /**
     * Returns the single view as HTML.
     *
     * @param array $piVars piVars array, must contain the key "showUid" with a valid object UID as value
     *
     * @return string HTML for the single view or an empty string if the
     *                provided UID is no UID of a valid realty object
     */
    public function render(array $piVars = array()) {

        $this->piVars = $piVars;

        $this->createHeatmapView();

        return $this->getSubpart('HEATMAP_VIEW');
    }

    private function createHeatmapView() {

        $dbResult = $this->getImmos();

        $js = 'var points = [';

        $databaseConnection = Tx_Oelib_Db::getDatabaseConnection();
        while (($row = $databaseConnection->sql_fetch_assoc($dbResult))) {
           //$js .= 'new google.maps.LatLng('.$row['latitude'].', '.$row['longitude'].'), ';
            $js .= '['.$row['latitude'].', '.$row['longitude'].'],';
        }
        $databaseConnection->sql_free_result($dbResult);

        $js = substr($js, 0, -1).'];';

        $this->setSubpart('javascript', $js, 'map');
    }

    private function getImmos() {
        $whereClause = $this->getWhereClausePartForPidList();

        $query = '(' .
        'SELECT latitude, longitude' .
        ' FROM tx_realty_objects' .
        ' WHERE 1=1 AND latitude > 0 AND longitude > 0' . $whereClause .
        ' LIMIT 10000000000000' .
        ')';

        $dbResult = Tx_Oelib_Db::getDatabaseConnection()->sql_query(
            $query
        );

        if ($dbResult === FALSE) {
            throw new tx_oelib_Exception_Database();
        }

        return $dbResult;
    }

    protected function getWhereClausePartForPidList() {
        $pidList = tx_oelib_db::createRecursivePageList(
            $this->getConfValueString('pages'),
            $this->getConfValueInteger('recursive')
        );

        return !empty($pidList)
            ? ' AND ' . 'tx_realty_objects' . '.pid IN (' . $pidList . ')'
            : '';
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/realty/pi1/class.tx_realty_pi1_SingleView.php']) {
    include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/realty/pi1/class.tx_realty_pi1_SingleView.php']);
}